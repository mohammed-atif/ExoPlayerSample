package www.zemoso.com.exoplayersample;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;

import java.io.Serializable;

/**
 * Created by adv on 20.10.2016.
 */

public class CircularFrameLayout extends FrameLayout implements Serializable {
    private static final long serialVersionUID = 6128016096756071380L;

    private static final String TAG = "CircularView";

    private static final int DIFF = 4;
    private static final int STROKE_WIDTH = (int) (DIFF * 0.5f);
    private static final int ALPHA = 128;

    private boolean isPrivate;
    private Paint paint;
    private boolean isCircular;
    private Path clippingPath;

    //region Constructors

    public CircularFrameLayout(Context context) {
        this(context, null);

    }

    public CircularFrameLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public CircularFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);

    }

    public CircularFrameLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        Log.d(TAG, "CircularView() called with: context = [" + context + "], attrs = [" + attrs + "], defStyleAttr = [" + defStyleAttr + "], defStyleRes = [" + defStyleRes + "]");

        paint = new Paint();
        paint.setColor(Color.CYAN);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(STROKE_WIDTH);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAlpha(ALPHA);

        setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));

        isCircular = true;
    }

    //endregion

    //region FrameLayout Inherited methods

    @Override
    protected void onDraw(Canvas canvas) {
        // Create a circular path.
        if (isCircular) {
            if (clippingPath != null && isPrivate){
                canvas.drawPath(clippingPath, paint);
            }
        }
        super.onDraw(canvas);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        if (isCircular) {
            int count = canvas.save();

            if (clippingPath != null)
                canvas.clipPath(clippingPath);

            super.dispatchDraw(canvas);
            canvas.restoreToCount(count);
        } else
            super.dispatchDraw(canvas);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        Log.d(TAG, "onSizeChanged() called with: w = [" + w + "], h = [" + h + "], oldw = [" + oldw + "], oldh = [" + oldh + "]");

        if (w != oldw || h != oldh) {
            updateClippingPath(w, h);
        }
    }

    //endregion

    //region helper methods

    private synchronized void updateClippingPath(int w, int h) {
        Log.d(TAG, "updateClippingPath() called with: w = [" + w + "], h = [" + h + "]");
        final int radius = Math.min(w, h) / 2;
        clippingPath = new Path();

        clippingPath.addCircle(radius + DIFF, radius + DIFF, radius - (DIFF * 2), Path.Direction.CW);
    }

    //endregion

    //region View Controller Methods

    public void init(boolean isCircular) {
        this.isCircular = isCircular;
    }

    public void setPrivate(boolean aPrivate) {
        Log.d(TAG, "setPrivate() called with: aPrivate = [" + aPrivate + "]");
        isPrivate = aPrivate;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void switchWindowMode(){
        init(!isCircular);
        invalidate();
    }

    public boolean isCircular() {
        return isCircular;
    }

    //endregion
}
