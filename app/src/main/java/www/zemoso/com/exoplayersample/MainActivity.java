package www.zemoso.com.exoplayersample;

import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.LoopingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Util;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity
        implements ExtractorMediaSource.EventListener, ExoPlayer.EventListener, View.OnTouchListener, GestureDetector.OnGestureListener{

    private final String TAG = MainActivity.class.getSimpleName();
    private TextureView mMainVideoView;
    private SimpleExoPlayer mExoPlayer;
    private FrameLayout rootView;
    private BandwidthMeter defaultBandwidthMeter;
    private GestureDetector detector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mMainVideoView = (TextureView) findViewById(R.id.main_video);
        mMainVideoView.setOnTouchListener(this);
        rootView = (FrameLayout) mMainVideoView.getParent();
        detector = new GestureDetector(this, this);
        Handler mainHandler = new Handler();
        mExoPlayer = ExoPlayerFactory.newSimpleInstance(this, getTrackSelector());
        mExoPlayer.setVideoTextureView(mMainVideoView);
        mExoPlayer.prepare(getMediaSource("IKEA.mp4", mainHandler));
        mExoPlayer.addListener(this);
        mExoPlayer.setPlayWhenReady(true);
        final FrameLayout parentComment = (FrameLayout) LayoutInflater.from(this)
                .inflate(R.layout.circular_video, null, false);
        final TextureView commentView = (TextureView) parentComment.findViewById(R.id.comment_video);
        final SimpleExoPlayer commentPlayer = ExoPlayerFactory.newSimpleInstance(this, getTrackSelector());
        commentPlayer.setVideoTextureView(commentView);
        commentPlayer.prepare(getMediaSource("redhead.mp4", new Handler()));
        int diameter = (int) getResources().getDimension(R.dimen.comment_diameter);
        final FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(diameter, diameter);
        params.gravity = Gravity.CENTER;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                rootView.addView(parentComment);
                parentComment.setLayoutParams(params);
//                parentComment.setPadding(4, 4, 4, 4);
                commentPlayer.setPlayWhenReady(true);
                commentView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        commentPlayer.setPlayWhenReady(!commentPlayer.getPlayWhenReady());
                    }
                });
//                mExoPlayer.setPlayWhenReady(false);
                parentComment.setAlpha(0.4f);
            }
        }, 2000);
    }

    private MediaSource getMediaSource(String videoPath, Handler mainHandler){
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getResources().getString(R.string.app_name)), (TransferListener<? super DataSource>) getDefaultBandwidthMeter());
    // Produces Extractor instances for parsing the media data.
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
    // This is the MediaSource representing the media to be played.
        Uri videoFile = Uri.parse("file:///android_asset/"+videoPath);
        return new LoopingMediaSource(new ExtractorMediaSource(videoFile,
                dataSourceFactory, extractorsFactory, mainHandler, this));
    }

    private TrackSelector getTrackSelector(){
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(getDefaultBandwidthMeter());
        return new DefaultTrackSelector(videoTrackSelectionFactory);
    }

    private BandwidthMeter getDefaultBandwidthMeter(){
        if(defaultBandwidthMeter == null){
            defaultBandwidthMeter = new DefaultBandwidthMeter();
        }
        return defaultBandwidthMeter;
    }

    @Override
    public void onLoadError(IOException error) {

    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity() {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    long lastSeek = System.currentTimeMillis();

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        long currenttime = System.currentTimeMillis();
        if(currenttime - lastSeek > 500) {
            lastSeek = currenttime;
            Log.d(TAG, "onScroll called with dx " + distanceX);
            int currentTime = (int) (mExoPlayer.getCurrentPosition() - distanceX * 5);
            int currentFrame = (int) (mExoPlayer.getCurrentWindowIndex() - distanceX);
//            mExoPlayer.seekToDefaultPosition(currentFrame < 0 ? 0 : currentFrame);
            mExoPlayer.seekTo(currentTime);
            return true;
        }else{
            Log.e(TAG, "onScroll called with dx " + distanceX);
        }
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN){
            mExoPlayer.setPlayWhenReady(false);
        }/*else if(event.getAction() == MotionEvent.ACTION_UP){
            mExoPlayer.setPlayWhenReady(true);
        }*/
        return detector.onTouchEvent(event);
    }
}
